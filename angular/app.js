var app = angular.module('app', ['ngResource', 'ngRoute', 'ngCookies']);

var settings = {
  'servername': 'ruokatauko.fi:8000',
  'mealamount': 25,
}

function loadNewMealsToScope(amount, $cookieStore, $rootScope, MealFactory) {
  var meals = MealFactory.query({'amount': amount}, function() {
    $cookieStore.put('meals', meals);
    $rootScope.meals = meals;
  });
}

/* CONFIG */

app.config(['$locationProvider', '$routeProvider', '$httpProvider',
  function($locationProvider, $routeProvider, $httpProvider) {

  $routeProvider
    .when('/', {
        templateUrl: 'query.html',
        controller: 'QueryController'
    })
    .when('/form', {
        templateUrl: 'form.html',

    })
    .otherwise({redirectTo : '/'});

    //$location.html5Mode(true); //now there won't be a hashbang within URLs for browers that support HTML5 history
}]);

app.run(['$http', '$cookieStore', '$rootScope', '$location', 'MealFactory', 'SessionService',
  function($http, $cookieStore, $rootScope, $location, MealFactory, SessionService) {

  // set meals
  if (!$cookieStore.get('meals') || ($cookieStore.get('meals').length == 0)) {
    SessionService.setNewSession();
    loadNewMealsToScope(settings.mealamount, $cookieStore, $rootScope, MealFactory);
  } else {
    $rootScope.meals = $cookieStore.get('meals');
  }
  $rootScope.totalamount = settings.mealamount;

  if ($cookieStore.get('formCompleted') === false)
    $location.path('/form');

}]);


/* RESOURCES */

app.factory('MealFactory', ['$resource', function($resource) {
  return $resource('http://' + settings.servername + '/api/v1/fi/meals.json', {},
    { 'get': {method: 'GET', isArray: true} });
}]);

app.factory('AnswerFactory', ['$resource', '$cookieStore', function($resource, $cookieStore) {
  return $resource('http://' + settings.servername + '/api/v1/fi/answer/', { }, {
    post: { method: 'POST' }
  });
}]);

app.factory('LotteryEntryFactory', ['$resource', '$cookieStore', function($resource, $cookieStore) {
  return $resource('http://' + settings.servername + '/api/v1/fi/lotteryentry/', {  }, {
    post: { method: 'POST' }
  });
}]);


/* SERVICES */

app.service('SessionService', ['$cookieStore', function($cookieStore) {
  this.setNewSession = function () {
    $cookieStore.put('queryid', makeid(30));
    $cookieStore.put('formCompleted', false);
  }
}]);


/* CONTROLLERS */

app.controller('NavbarController', ['$scope', '$location', function($scope, $location) {
  $scope.isActive = function (viewLocation) {
    return viewLocation == $location.path();
  }
}]);

app.controller('QueryController',
  ['$rootScope', '$scope', 'MealFactory', 'AnswerFactory', '$cookieStore', '$timeout', '$location',
  function($rootScope, $scope, MealFactory, AnswerFactory, $cookieStore, $timeout, $location) {

  // initialization
  $scope.$watch('meals', function() {
    if ($scope.meals)
      $scope.meal = $scope.meals[0];
    else
      $scope.meal = "";
    $timeout(function() {
        $scope.answers = settings.mealamount - $scope.meals.length;
        if ($scope.meals.length == 0)
          $scope.progress = 100;
        else
         $scope.progress = $scope.answers / ($scope.answers + $scope.meals.length) * 100;
    }, 100);
    $cookieStore.put('meals', $scope.meals)
  }, true);

  $scope.answer = function(meal, answer) {
    if ($scope.meals <= 0)
      return;
    AnswerFactory.post({ "meal": meal.id, "answer": answer, "queryid": $cookieStore.get('queryid') })

    $scope.meals.shift(meal); // pops first item

    if (answer == 2) {
      var newmeal = MealFactory.query({'amount': 1}, function() {
        $scope.meals.push(newmeal[0]);
      });
      return;
    }

    if ($scope.meals.length == 0) {
      $timeout(function() {
        $scope.thanksmessage = true;
        $cookieStore.put('formCompleted', true);
      }, 200);
      $timeout(function() {
        $scope.thanksmessage = false;
        $location.path('/form');
      }, 1500);
    }
  }
}]);

app.controller('FormController',
  ['$cookieStore', '$rootScope', '$scope', '$location', 'LotteryEntryFactory', 'MealFactory', 'SessionService',
  function($cookieStore, $rootScope, $scope, $location, LotteryEntryFactory, MealFactory, SessionService) {

  if (!$cookieStore.get('formCompleted'))
    $location.path('/');

  $scope.inputform = true;

  // default-params
  $scope.validEmail = true;
  $scope.validName = true;

  $scope.send = function(name, email) {
    $scope.validName = name;
    $scope.validEmail = email;
    if (!name || !email) return;

    LotteryEntryFactory.post({ "name": name, "email": email, "queryid": $cookieStore.get('queryid') });
    $cookieStore.put('formCompleted', false);
    $scope.inputform = false;
  }

  $scope.newLottery = function() {
    SessionService.setNewSession();
    loadNewMealsToScope(settings.mealamount, $cookieStore, $rootScope, MealFactory);
    $location.path('/');
  }

}]);


/* UTILITIES */

function makeid(length)
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for( var i=0; i < length; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}
