from django.db import models

class Meal(models.Model):

    name = models.CharField(max_length=255)

    def __unicode__(self):
        return u'%s' % self.name


class Answer(models.Model):

    answer = models.SmallIntegerField(default=2, choices=((0, "NOOO"), (1, "No"), (2, "-"), (3, "Yes"), (4, "YEEAAH")))
    meal = models.ForeignKey('Meal')
    ip = models.CharField(max_length=45, blank=True)
    queryid = models.CharField(max_length=255, blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)


class LotteryEntry(models.Model):

    name = models.CharField(max_length=255)
    email = models.CharField(max_length=511)
    ip = models.CharField(max_length=45, blank=True)
    queryid = models.CharField(max_length=255, blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)

