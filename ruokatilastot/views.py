from ruokatilastot.models import Meal, Answer, LotteryEntry
from ruokatilastot.serializers import MealSerializer, AnswerSerializer, LotteryEntrySerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import AllowAny
from math import ceil, floor
from itertools import chain
import random


class Meals(APIView):

    def get(self, request, format=None):

        if request.GET.get("amount"):
            amount = int(request.GET.get("amount"))
        else:
            amount = 1

        meals = Meal.objects.all()

        if amount == 1:
            meals_return = meals.order_by('?')[:amount]
        else:
            meals_old = meals.filter(id__lte=36)[:floor(amount/3.0)]
            meals_new = meals.filter(id__gte=37)[:ceil(2*amount/3.0)]
            meals_return = list(chain(meals_old, meals_new))
            random.shuffle(meals_return)

        serializer = MealSerializer(meals_return, many=True)
        return Response(serializer.data)


class Answer(APIView):
    permission_classes = (AllowAny,) # Allow non-authenticated users to post

    def post(self, request, format=None):
        data = request.DATA
        data["ip"] = get_client_ip(request)

        serializer = AnswerSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LotteryEntry(APIView):
    permission_classes = (AllowAny,) # Allow non-authenticated users to post

    def post(self, request, format=None):
        data = request.DATA
        data["ip"] = get_client_ip(request)

        serializer = LotteryEntrySerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip
