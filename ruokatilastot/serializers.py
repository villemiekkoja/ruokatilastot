from rest_framework import serializers
from ruokatilastot.models import Meal, Answer, LotteryEntry


class MealSerializer(serializers.ModelSerializer):

	class Meta:
		model = Meal
		fields = ('id', 'name')


class AnswerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Answer
        fields = ('meal', 'answer', 'ip', 'queryid')


class LotteryEntrySerializer(serializers.ModelSerializer):

    class Meta:
        model = LotteryEntry
        fields = ('name', 'email', 'ip', 'queryid')
