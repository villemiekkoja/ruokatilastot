import string
import random

def generate_random_string(length):
    return "".join([random.choice(string.letters+string.digits) for x in range(1, length)])
