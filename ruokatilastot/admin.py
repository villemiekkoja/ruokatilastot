from django.contrib import admin
from models import Meal, Answer, LotteryEntry

class MealAdmin(admin.ModelAdmin):
	list_display = ('name',)

class AnswerAdmin(admin.ModelAdmin):
	list_display = ( 'meal', 'answer', 'ip', 'queryid', 'timestamp')

class LotteryEntryAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'ip', 'queryid', 'timestamp')


admin.site.register(Meal, MealAdmin)
admin.site.register(Answer, AnswerAdmin)
admin.site.register(LotteryEntry, LotteryEntryAdmin)
