# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Meal'
        db.create_table(u'ruokatilastot_meal', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'ruokatilastot', ['Meal'])

        # Adding model 'Answer'
        db.create_table(u'ruokatilastot_answer', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('answer', self.gf('django.db.models.fields.SmallIntegerField')(default=2)),
            ('meal', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['ruokatilastot.Meal'])),
            ('ip', self.gf('django.db.models.fields.CharField')(max_length=45, blank=True)),
            ('queryid', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'ruokatilastot', ['Answer'])

        # Adding model 'LotteryEntry'
        db.create_table(u'ruokatilastot_lotteryentry', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=511)),
            ('ip', self.gf('django.db.models.fields.CharField')(max_length=45, blank=True)),
            ('queryid', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'ruokatilastot', ['LotteryEntry'])


    def backwards(self, orm):
        # Deleting model 'Meal'
        db.delete_table(u'ruokatilastot_meal')

        # Deleting model 'Answer'
        db.delete_table(u'ruokatilastot_answer')

        # Deleting model 'LotteryEntry'
        db.delete_table(u'ruokatilastot_lotteryentry')


    models = {
        u'ruokatilastot.answer': {
            'Meta': {'object_name': 'Answer'},
            'answer': ('django.db.models.fields.SmallIntegerField', [], {'default': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'}),
            'meal': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ruokatilastot.Meal']"}),
            'queryid': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'ruokatilastot.lotteryentry': {
            'Meta': {'object_name': 'LotteryEntry'},
            'email': ('django.db.models.fields.CharField', [], {'max_length': '511'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'queryid': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'ruokatilastot.meal': {
            'Meta': {'object_name': 'Meal'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['ruokatilastot']