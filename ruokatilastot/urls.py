from django.conf.urls import patterns, include, url
from django.contrib import admin
admin.autodiscover()
from ruokatilastot import views

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += patterns('ruokatilastot.views',
    url(r'^api/v1/fi/meals\.json/?$', views.Meals.as_view()),           # GET
    url(r'^api/v1/fi/answer/?$', views.Answer.as_view()),               # POST
    url(r'^api/v1/fi/lotteryentry/?$', views.LotteryEntry.as_view()),   # POST
)
